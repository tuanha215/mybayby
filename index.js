var express = require('express');
var http = require('http');   
var app = express();
var bodyParser = require('body-parser');
var engine = require('ejs-locals');
var Sequelize = require('sequelize');
var db = require('./models/db');

app.use(bodyParser.urlencoded({ extended: true }));

app.engine('ejs', engine);
app.set("views", "./views")
app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(express.static(__dirname + '/node_modules/bootstrap/dist'));

http.createServer(app).listen(process.env.PORT || 3000);

db.sequelize.sync().then(() => {});

const homeController = require('./controllers/home');

app.post ("/", function(require, response){
  a = require.body.name
  response.send('a' + a)
})

app.get('/', homeController.index);